/**
 * This file is where you define your application routes and controllers.
 * 
 * Start by including the middleware you want to run for every request;
 * you can attach middleware to the pre('routes') and pre('render') events.
 * 
 * For simplicity, the default setup for route controllers is for each to be
 * in its own file, and we import all the files in the /routes/views directory.
 * 
 * Each of these files is a route controller, and is responsible for all the
 * processing that needs to happen for the route (e.g. loading data, handling
 * form submissions, rendering the view template, etc).
 * 
 * Bind each route pattern your application should respond to in the function
 * that is exported from this module, following the examples below.
 * 
 * See the Express application routing documentation for more information:
 * http://expressjs.com/api.html#app.VERB
 */

var keystone = require('keystone');
var middleware = require('./middleware');
var importRoutes = keystone.importer(__dirname);

// Common Middleware
keystone.pre('routes', middleware.initLocals);
keystone.pre('render', middleware.flashMessages);

keystone.set('404', function (req, res, next) {
	res.status(404).render('errors/404');
});

// Import Route Controllers
var routes = {
	views: importRoutes('./views')
};

// Setup Route Bindings
exports = module.exports = function(app) {
	
	// Views
	app.get('/', routes.views.index);
	app.get('/booking', routes.views.booking);
	app.get('/booking/detail/:slug', routes.views.bookingdetail);
	app.get('/booking/:region', routes.views.bookingregion);
	app.get('/news', routes.views.news);
	app.get('/location', routes.views.location);
	app.get('/location/detail/:slug', routes.views.locationdetail);
	app.get('/location/:region', routes.views.locationregion);
	app.all('/location/search', routes.views.locationsearch);
	app.get('/contact', routes.views.contact);
	app.post('/contact', routes.views.contactpost);
	app.get('/team', routes.views.team);
	app.get('/purpose', routes.views.purpose);
	app.get('/terms', routes.views.term);
	app.get('/privacy-policy', routes.views.privacy);
	app.get('/services', routes.views.services);
	app.get('/careers', routes.views.career);
	app.all('/careers/apply/:key', routes.views.careerapply);
	app.post('/careers/apply', routes.views.careerpost);
	app.get('/press', routes.views.press);
	app.get('/press/recent', routes.views.pressrecent);
	app.get('/press/archive', routes.views.pressarchive);
	app.get('/warehouse', routes.views.warehouse);
	app.get('/adp', routes.views.adp);
	app.get('/referrals', routes.views.referrals);
	app.get('/b4t', routes.views.b4t);
	app.get('/', routes.views.index);
	
	// NOTE: To protect a route so that only admins can see it, use the requireUser middleware:
	// app.get('/protected', middleware.requireUser, routes.views.protected);
	
};
