/**
 * Created by balpreet on 12/24/2015.
 */
var keystone = require('keystone');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;

    // Set locals
    //locals.section = 'purpose';

    view.query('purposes', keystone.list('Purpose').model.find().where('state', 'published').sort('sortOrder'));
    view.query('titles', keystone.list('Title').model.find().where('page_name', 'purpose').limit('1'));

    view.query('footernavs', keystone.list('Footer_Nav').model.find().where('state', 'published').sort('sortOrder'));
    // Render the view
    view.render('purpose');

};