var keystone = require('keystone');

exports = module.exports = function(req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;
	
	// Set locals
	locals.section = 'homepage';
	view.query('titles', keystone.list('Title').model.find().where('page_name', 'homepage').limit('1'));

	view.query('homepages', keystone.list('Homepage').model.find().sort('sortOrder'));
	
	// Render the view
	view.render('index');

};
