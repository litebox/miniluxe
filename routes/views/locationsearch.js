var keystone = require('keystone');

exports = module.exports = function(req, res){
    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.formData = req.body || {};
    locals.data = {
        locations: [],
        invalid: []
    };

    view.on('init', function(next) {
        //var re = new RegExp(req.body.search, 'i');
        if (isNaN(req.body.search)){
            //var postsQuery = keystone.list('Location').model.find({ 'nameLong': req.body.search });
            keystone.list('Location').model.find({city: new RegExp('^'+req.body.search+'$', "i")}, function(err, results) {
                //Do your action here..
                locals.data.locations = results;
                if (results == ''){
                    locals.data.invalid = 'We are not serving in this area';
                }
                next(err);
            });
        } else{
            keystone.list('Location').model.find({zipcode: new RegExp('^'+req.body.search+'$', "i")}, function(err, results) {
                //Do your action here..
                locals.data.locations = results;
                if (results == ''){
                    locals.data.invalid = 'We are not serving in this area';
                }
                next(err);
            });
        }

    });

    view.query('regions', keystone.list('Region').model.find().sort('name'));
    view.query('footernavs', keystone.list('Footer_Nav').model.find().where('state', 'published').sort('sortOrder'));

    view.render('locationsearch');
};