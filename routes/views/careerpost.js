var keystone = require('keystone');
var nodemailer=require('nodemailer');
var mandrillTransport = require('nodemailer-mandrill-transport');
var mandrill = require('mandrill-api/mandrill');
var mandrill_client = new mandrill.Mandrill('ChXn4zC6ZP4YmzHmFRax3A');
var fs=require('fs');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.formData = req.body || {};

    locals.validationErrors = {};
    locals.enquirySubmitted = false;
    // Set locals
    //locals.section = 'location';
    locals.filters = {
        key: req.params.key
    };

    function base64_encode(file) {
        // read binary data
        var bitmap = fs.readFileSync(file);
        // convert binary data to base64 encoded string
        return new Buffer(bitmap).toString('base64');
    }

    if (req.files.resume){
        var base64str = base64_encode(req.files.resume.path);
        var extension = req.files.resume.extension;
        var message = {
            "html": "From:"+locals.formData.email+"<br>First Name:"+locals.formData.first_name+"<br>Last Name:"+locals.formData.last_name+"<br>Area:"+locals.formData.area+"<br>Home Zip Code:"+locals.formData.postal_code+"<br>Phone:"+locals.formData.phone+"<br>Nail License:"+locals.formData.license_nail_service+"<br>Wax License:"+locals.formData.license_waxing_service+"<br>Applied_For:"+locals.formData.applied_for+"",
            "subject": "Miniluxe Career Application",
            "from_email": locals.formData.email,
            "from_name": locals.formData.first_name,
            "to": [{
                "email": "careers@miniluxe.com", //careers@miniluxe.com
                "name": "Miniluxe",
                "type": "to"
            }],
            "headers": {
                "Reply-To": locals.formData.email
            },
            "important": true,
            "attachments": [{
                "type": "text/"+extension,
                "name": "resume."+extension,
                "content": base64str
            }]
        };
    } else{
        var message = {
            "html": "From:"+locals.formData.email+"<br>First Name:"+locals.formData.first_name+"<br>Last Name:"+locals.formData.last_name+"<br>Area:"+locals.formData.area+"<br>Home Zip Code:"+locals.formData.postal_code+"<br>Phone:"+locals.formData.phone+"<br>Nail License:"+locals.formData.license_nail_service+"<br>Wax License:"+locals.formData.license_waxing_service+"<br>Applied_For:"+locals.formData.applied_for+"",
            "subject": "Miniluxe Career Application",
            "from_email": locals.formData.email,
            "from_name": locals.formData.first_name,
            "to": [{
                "email": "careers@miniluxe.com", //careers@miniluxe.com
                "name": "Miniluxe",
                "type": "to"
            }],
            "headers": {
                "Reply-To": locals.formData.email
            },
            "important": true
        };
    }


    var async = true;
    mandrill_client.messages.send({"message": message, "async": async}, function(result) {
        //console.log(result);
        locals.enquirySubmitted = true;
        // Render the view
        view.render('careerthanks');
        /*
         [{
         "email": "recipient.email@example.com",
         "status": "sent",
         "reject_reason": "hard-bounce",
         "_id": "abc123abc123abc123abc123abc123"
         }]
         */
    }, function(e) {
        // Mandrill returns the error as an object with name and message keys
        console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
        locals.enquirySubmitted = false;
        view.render('careerthanks');
        // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
    });

    locals.enquirySubmitted = true;

    view.query('careerapply', keystone.list('Vacancy').model.find().where('key', locals.filters.key));
    view.query('careers', keystone.list('Careers').model.find().sort('sortOrder'));
    view.query('titles', keystone.list('Title').model.find().where('page_name', 'career post').limit('1'));

    view.query('footernavs', keystone.list('Footer_Nav').model.find().where('state', 'published').sort('sortOrder'));

    // Render the view
    //view.render('careerthanks');

};
