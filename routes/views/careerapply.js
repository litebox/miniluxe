var keystone = require('keystone');
var nodemailer=require('nodemailer');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.formData = req.body || {};

    locals.validationErrors = {};
    locals.enquirySubmitted = false;
    // Set locals
    //locals.section = 'location';
    locals.filters = {
        key: req.params.key
    };




    view.query('careerapply', keystone.list('Vacancy').model.find().where('key', locals.filters.key));
    view.query('careers', keystone.list('Careers').model.find().sort('sortOrder').limit('1'));
    view.query('positions', keystone.list('Career_position').model.find().where('state', 'published').sort(''));
    view.query('locations', keystone.list('Location').model.find().sort('sequence'));
    view.query('states', keystone.list('State').model.find().where('state', 'published').sort(''));
    view.query('titles', keystone.list('Title').model.find().where('page_name', 'career apply').limit('1'));

    view.query('educations', keystone.list('Education').model.find().where('state', 'published').sort(''));
    view.query('footernavs', keystone.list('Footer_Nav').model.find().where('state', 'published').sort('sortOrder'));

    // Render the view
    view.render('careerapply');

};
