var keystone = require('keystone');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.data = {
        locations: []
    };
    // Set locals
    //locals.section = 'location';

    //view.query('locations', keystone.list('Location').model.find().sort('sequence'));
    view.on('init', function(next) {

        var q = keystone.list('Location').paginate({
                page: req.query.page || 1,
                perPage: 100,
                maxPages: 10
            })
            .sort();

        //if (locals.data.category) {
        //    q.where('categories').in([locals.data.category]);
        //}

        q.exec(function(err, results) {
            locals.data.locations = results;
            next(err);
        });

    });
    view.query('regions', keystone.list('Region').model.find().where('state', 'published').sort('name'));
    view.query('titles', keystone.list('Title').model.find().where('page_name', 'locations').limit('1'));

    view.query('footernavs', keystone.list('Footer_Nav').model.find().where('state', 'published').sort('sortOrder'));
    // Render the view
    view.render('location');

};
