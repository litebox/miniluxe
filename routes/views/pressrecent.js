var keystone = require('keystone');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;
        locals.data = {
        pressrecents: []
    };

    // Set locals
    locals.section = 'press';

    // Load the news by sortOrder
    //view.query('pressrecents', keystone.list('Press').model.findOne().sort('-publishedDate'));
    //keystone.list('Press').model.find({}, {}, { sort: { 'publishedDate' : -1 } }, function(err, post) {
    //    locals.data.pressrecents = post;
    //});
    view.on('init', function(next) {

        var q = keystone.list('Press').paginate({
                page: req.query.page || 1,
                perPage: 6,
                maxPages: 10
            })
            .sort('publishedDate', '-1');

        q.exec(function(err, results) {
            locals.data.pressrecents = results;
            next(err);
        });

    });
    view.query('footernavs', keystone.list('Footer_Nav').model.find().where('state', 'published').sort('sortOrder'));
    // Render the view
    view.render('pressrecent');

};
