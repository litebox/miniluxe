var keystone = require('keystone');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;

    // Set locals
    locals.section = 'new';

    // Load the news by sortOrder
    view.query('news', keystone.list('News').model.find().sort('publishedDate'));
    //view.query('titles', keystone.list('Title').model.find().where('page_name', 'news').limit('1'));

    view.query('footernavs', keystone.list('Footer_Nav').model.find().where('state', 'published').sort('sortOrder'));
    // Render the view
    view.render('news');

};
