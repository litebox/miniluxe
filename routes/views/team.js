var keystone = require('keystone');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;

    // Set locals
    //locals.section = 'contact';
    view.query('teams', keystone.list('Team').model.find({ active:true}).sort(''));
    view.query('footernavs', keystone.list('Footer_Nav').model.find().where('state', 'published').sort('sortOrder'));
    view.query('titles', keystone.list('Title').model.find().where('page_name', 'team').limit('1'));

    // Render the view
    view.render('team');

};
