var keystone = require('keystone');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;


    // Set locals
    locals.section = 'service';
    locals.filters = {
        service: req.params.service
    };
    locals.data = {
        subservices: [],
        services: []
    };
    view.query('subservices', keystone.list('Subservice').model.find().where('state', 'published').populate('service').sort('sortOrder'));
    view.query('addons', keystone.list('Add_on').model.find().where('state', 'published').populate('service').sort('sortOrder'));
    view.query('faces', keystone.list('Face').model.find().where('state', 'published').populate('service'));
    view.query('titles', keystone.list('Title').model.find().where('page_name', 'service').limit('1'));

    view.query('servicemenus', keystone.list('Download_Servicemenu').model.find().where('state', 'published').limit('1'));
    //view.on('init', function(next) {
    //
    //    var q = keystone.list('Subservice').paginate({
    //            page: req.query.page || 1,
    //            perPage: 100,
    //            maxPages: 100
    //        })
    //        .where('state', 'published')
    //        .populate('author service');
    //
    //    if (locals.data.service) {
    //        q.where('services').in([locals.data.service]);
    //    }
    //
    //    q.exec(function(err, results) {
    //        locals.data.subservices = results;
    //        console.log(locals.data.subservices);
    //        next(err);
    //    });
    //
    //});

    view.query('services', keystone.list('Service').model.find().where('state', 'published').sort(''));

    view.query('footernavs', keystone.list('Footer_Nav').model.find().where('state', 'published').sort('sortOrder'));
    // Render the view
    view.render('services');

};