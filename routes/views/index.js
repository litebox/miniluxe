var keystone = require('keystone');

exports = module.exports = function(req, res) {
	
	var view = new keystone.View(req, res);
	var locals = res.locals;
	
	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'homepage';
	view.query('titles', keystone.list('Title').model.find().where('page_name', 'homepage').limit('1'));
	view.query('homepages', keystone.list('Homepage').model.find().where('state', 'published').sort().limit('1'));
	view.query('sliders', keystone.list('Slider').model.find().where('state', 'published').sort());
	view.query('footernavs', keystone.list('Footer_Nav').model.find().where('state', 'published').sort('sortOrder'));
	// Render the view
	view.render('index');
	
};
