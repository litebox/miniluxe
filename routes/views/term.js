/**
 * Created by balpreet on 1/6/2016.
 */

var keystone = require('keystone');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;

    // Set locals
    locals.section = 'term';

    view.query('terms', keystone.list('Terms_Conditions').model.find({ active:true}).sort(''));
    view.query('footernavs', keystone.list('Footer_Nav').model.find().where('state', 'published').sort('sortOrder'));
    view.query('titles', keystone.list('Title').model.find().where('page_name', 'terms and conditions').limit('1'));

    // Render the view
    view.render('terms');

};