var keystone = require('keystone');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;

    // Set locals
    //locals.section = 'booking';
    view.query('giftcards', keystone.list('Gift_card').model.find().where('state', 'published').sort(''));
    view.query('subservices', keystone.list('Subservice').model.find().where('state', 'published').populate('service'));
    view.query('addons', keystone.list('Add_on').model.find().where('state', 'published').populate('service'));
    view.query('footernavs', keystone.list('Footer_Nav').model.find().where('state', 'published').sort('sortOrder'));
    // Render the view
    view.render('giftcard');
};