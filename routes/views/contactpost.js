var keystone = require('keystone');
//var Enquiry = keystone.list('Enquiry');
var nodemailer=require('nodemailer');
var mandrillTransport = require('nodemailer-mandrill-transport');

exports = module.exports = function(req, res) {
    var view = new keystone.View(req, res);
    var locals = res.locals;

    locals.section = 'contactthanks';
    //locals.enquiryTypes = Enquiry.fields.store_location.ops;

    locals.formData = req.body || {};

    locals.validationErrors = {};
    locals.enquirySubmitted = false;
    var transport = nodemailer.createTransport(mandrillTransport({
        auth: {
            apiKey: 'IelBD33mdK4Kqolva7mOdQ'
        }
    }));

    transport.sendMail({
        host: "smtp.mandrillapp.com",
        port:  587,
        from: locals.formData.email, // sender address
        to: 'info@miniluxe.com', // list of receivers info@miniluxe.com
        subject: 'Miniluxe Enquiry', // Subject line
        html: 'New enquiry on contact form <br>From:'+locals.formData.email+'<br>First Name:'+locals.formData.first_name+'<br>Last Name:'+locals.formData.last_name+'<br>Phone:'+locals.formData.phone+'<br>Store Location:'+locals.formData.store_location+'<br>Message:'+locals.formData.message+''
    }, function(err, info) {
        if (err) {
            var msg="email is not send due to some error";
            locals.enquirySubmitted = false;
            view.render('contactthanks');
        } else {
            locals.enquirySubmitted = true;
            view.render('contactthanks');
        }
    });
    view.query('footernavs', keystone.list('Footer_Nav').model.find().where('state', 'published').sort('sortOrder'));
    view.query('titles', keystone.list('Title').model.find().where('page_name', 'contact post').limit('1'));

};