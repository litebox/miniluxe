var keystone = require('keystone');
//var stripeSecretKey = keystone.get('sk_test_B3afcRHATiEKKN2XG9xK9CJT');
//var Payment = keystone.list('Payment');
var Stripe = require("stripe")(
    "sk_test_0d0OE5YdCoiB5aiGA3SvLNV5"
);

exports = module.exports = function(req, res){

    var view = new keystone.View(req, res);
    var stripeToken = req.body.stripeToken;
    var stripe = req.body.stripeEmail;
    var Tokentype = req.body.stripeTokenType;
    var locals = res.locals;
    locals.enquirySubmitted = false;
    var application = new Payment.model();
    locals.data = {
        services: [],
        amount: []
    };
    var final_amount = (req.body.amount * 100);
    //console.log(req.body);

    var charge = Stripe.charges.create({
        amount: final_amount,
        currency: 'USD',
        source: stripeToken,
        description: 'Charge for Miniluxe Gift card',
    }, function(err, charge) {
        if (err && err.type === 'StripeCardError') {
            response.json({ accepted: false, message: 'Payment Declined'})
            // The card has been declined
        } else {
            if (err) {
                console.log("We have an error!")
                console.log(err)
                response.json({ accepted: false, message: 'Payment Declined'})
                locals.enquirySubmitted = false;
                view.render('paymentthanks');
            } else {
                // The card has been accepted
                //console.log(charge)
                //console.log(charge.status);
                if (charge.status == 'succeeded'){

                    var newWord = new Payment.model({
                        From: req.body.from,
                        To: req.body.to,
                        Email: req.body.email,
                        Service: req.body.sel_services,
                        Message: req.body.message,
                        Amount: req.body.amount,
                        Card_Quantity: req.body.quantity,
                        Stripe_Token: stripeToken,
                        Gift_Card_Url: req.body.img_url,
                        Send_Card_By_Myself: req.body.send_card_by_myself
                    });
                    newWord.save();
                    locals.enquirySubmitted = true;
                    locals.data.services = req.body.sel_services;
                    locals.data.amount = req.body.amount;
                    view.render('paymentthanks');
                }
            }
        }
    })

};