var keystone = require('keystone');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;

    // Set locals
    //locals.section = 'booking';
    view.query('careers', keystone.list('Careers').model.find().where('state', 'published').sort('sortOrder').limit('1'));
    view.query('titles', keystone.list('Title').model.find().where('page_name', 'careers').limit('1'));
    view.query('vacancies', keystone.list('Vacancy').model.find().sort('sortOrder'));
    view.query('footernavs', keystone.list('Footer_Nav').model.find().where('state', 'published').sort('sortOrder'));
    // Render the view
    view.render('career');
};