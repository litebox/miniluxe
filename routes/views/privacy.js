/**
 * Created by balpreet on 1/6/2016.
 */
var keystone = require('keystone');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;

    // Set locals
    locals.section = 'privacy';

    view.query('privacies', keystone.list('Privacy').model.find({ active:true}).sort('sequence').limit(1));
    view.query('titles', keystone.list('Title').model.find().where('page_name', 'privacy policy').limit('1'));

    view.query('footernavs', keystone.list('Footer_Nav').model.find().where('state', 'published').sort('sortOrder'));
    // Render the view
    view.render('privacy');

};