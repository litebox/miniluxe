var keystone = require('keystone');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;

    // Set locals
    res.redirect('https://workforcenow.adp.com/public/index.htm');

};