var keystone = require('keystone');
//var Enquiry = keystone.list('Enquiry');
var nodemailer=require('nodemailer');
var mandrillTransport = require('nodemailer-mandrill-transport');


exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;

    locals.section = 'contact';
    view.query('titles', keystone.list('Title').model.find().where('page_name', 'contact').limit('1'));

    view.query('locations', keystone.list('Location').model.find().sort('sequence'));
    view.query('footernavs', keystone.list('Footer_Nav').model.find().where('state', 'published').sort('sortOrder'));
    view.render('contact', {
        section: 'contact',
    });

    //var view = new keystone.View(req, res);
    //var locals = res.locals;
    //
    //// Set locals
    ////locals.section = 'contact';
    //
    //view.query('footernavs', keystone.list('FooterNav').model.find().where('state', 'published').sort('Sequence'));
    //// Render the view
    //view.render('contact');

};
