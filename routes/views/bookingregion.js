var keystone = require('keystone');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;

    // Set locals
    locals.section = 'locationregion';
    locals.filters = {
        region: req.params.region
    };
    locals.data = {
        locations: [],
        region: []
    };

    view.on('init', function(next) {

        if (req.params.region) {
            keystone.list('Region').model.findOne({ key: locals.filters.region }).exec(function(err, result) {
                locals.data.region = result;
                next(err);
            });
        } else {
            next();
        }

    });
    view.on('init', function(next) {

        var q = keystone.list('Location').paginate({
                page: req.query.page || 1,
                perPage: 100,
                maxPages: 10
            })
            .populate('author region');

        if (locals.data.region) {
            q.where('region', locals.data.region);
        }

        q.exec(function(err, results) {
            locals.data.locations = results;
            next(err);
        });

    });
    view.query('regions', keystone.list('Region').model.find().where('state', 'published').sort('name'));
    view.query('titles', keystone.list('Title').model.find().where('page_name', 'location region').limit('1'));

    view.query('footernavs', keystone.list('Footer_Nav').model.find().where('state', 'published').sort('sortOrder'));

    // Render the view
    view.render('bookingregion');

};