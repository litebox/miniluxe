/**
 * Created by balpreet on 1/6/2016.
 */
/**
 * Created by balpreet on 1/6/2016.
 */
var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Slider Model
 * =============
 */

var Privacy = new keystone.List('Privacy', {
    autokey: { from: 'name', path: 'key', unique: true }
});

Privacy.add({
    name: { type: String, required: true },
    heroImage: { type: Types.CloudinaryImage, label:'HeroImage (811x200)' },
    herourl: {type: Types.Url, hidden: true},
    text: {type: Types.Html, wysiwyg: true, height: 150},
    sequence:{type:Number}
});

Privacy.schema.pre('save', function(next) {
    this.herourl = this._.heroImage.scale(811, 200,{ quality: 100});


    next();
});

Privacy.register();