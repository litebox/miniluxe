var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Servicemenu Model
 **/

var Download_Servicemenu = new keystone.List('Download_Servicemenu',{
    nocreate: true,
    nodelete: true,
    autokey: { from: 'name', path: 'key', unique: true }
});

Download_Servicemenu.add({
    name: { type: String, required: true, validate:[{ validator:isValidName, msg:"Only alphabetic characters are allowed" }]},
    state: { type: Types.Select, options: 'draft, published', default: 'published', index: true },
    File: { type: Types.LocalFile, dest: 'public/uploads/menu' }
});
Download_Servicemenu.defaultColumns = 'name, state|20%';

function isValidName(value, done) {
    var alphaonly=/^[a-zA-Z ]*$/
    if(value){
        if (!alphaonly.test(value)) {
            return done(null)

        }
        return done(1);
    }
    return done();
}

Download_Servicemenu.register();