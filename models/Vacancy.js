/**
 * Created by balpreet on 1/12/2016.
 */

var keystone = require('keystone'),
    Types = keystone.Field.Types;

var Vacancy = new keystone.List('Vacancy', {
    map: { name: 'title' },
    autokey: { from: 'title', path: 'key', unique: true },
    sortable: true
});
Vacancy.add({
    title: { type:String },
    description_block1: {type: Types.Textarea, height: 150},
    description_block2: {type: Types.Textarea, height: 150},
    Requirement: {type: Types.Html, wysiwyg: true, height: 150}
});

Vacancy.register();