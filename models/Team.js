/**
 * Created by balpreet on 1/6/2016.
 */
var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Team Model
 * =============
 */

var Team = new keystone.List('Team', {
    autokey: { from: 'name', path: 'key', unique: true }
});

Team.add({
    name: { type: String, required: true },
    heroImage: { type: Types.CloudinaryImage, label:'HeroImage (760x188)' },
    herourl: {type: Types.Url, hidden: true},
    text: {type: Types.Html, wysiwyg: true, height: 150}
});

Team.schema.pre('save', function(next) {
    this.herourl = this._.heroImage.scale(760, 188,{ quality: 100});


    next();
});


Team.register();