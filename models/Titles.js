/**
 * Created by balpreet on 2/7/2016.
 */
var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Footer Nav Model
 * =============
 */

var Title = new keystone.List('Title', {
    autokey: { from: 'name', path: 'key', unique: true }
});

Title.add({
    name: { type: String, required: true },

    page_name: { type: Types.Select, options: 'homepage,service,locations,purpose,careers,book an appointment,team,contact,privacy policy,terms and conditions,career apply,career post,contact post,location detail,location region', index: true },
});

Title.defaultColumns = 'name, state|20%, page_name|20%';
Title.register();