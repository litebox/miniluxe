/**
 * Created by balpreet on 1/12/2016.
 */
var keystone = require('keystone'),
    Types = keystone.Field.Types;

var Careers = new keystone.List('Careers', {
    nocreate: true,
    nodelete: true,
    map: { name: 'title' },
    autokey: { from: 'title', path: 'key', unique: true }
});
Careers.add({
    title: { type:String, validate:[{ validator:isValidName, msg:"Please enter character only" }] },
    state: { type: Types.Select, options: 'draft, published', default: 'draft', index: true },
    description: {type: Types.Html, wysiwyg: true, height: 150}
});

Careers.defaultColumns = 'title, state|20%';
function isValidName(value, done) {
    var alphaonly=/^[a-zA-Z ]*$/
    if(value){
        if (!alphaonly.test(value)) {
            return done(null)

        }
        console.log('ltv');
        return done(1);
    }
    return done();
}
Careers.register();