/**
 * Created by balpreet on 12/24/2015.
 */
var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Homepage Model
 **/

var Purpose = new keystone.List('Purpose',{
    map: { name: 'name' },
    autokey: { from: 'name', path: 'slug', unique: true }

});

Purpose.add({
    name: { type: String, default: 'purpose', required: true },
    state: { type: Types.Select, options: 'draft, published', default: 'published', index: true },
    //author: { type: Types.Relationship, ref: 'Y', index: true },
    //publishedDate: { type: Date, default: Date.now },
    bannerImage: { type: Types.CloudinaryImage, label:'Banner Image (2000x650)' },
    description: { type: String, required: false },
    BlockTitle: { type: String, required: false },
    //purpose:       {type: Types.Textarea},
    Block1: {
        title: { type: String, required: false },
        description: { type: String, required: false, label:'Block1 description(max:180)' },
        image: { type: Types.CloudinaryImage, label:'Block1 image (710x418)' }
    },
    Block2: {
        title: { type: String, required: false },
        description: { type: String, required: false, label:'Block2 description(max:180)' },
        image: { type: Types.CloudinaryImage, label:'Block1 image (538x572)' }
    },
    Block3: {
        title: { type: String, required: false },
        description: { type: String, required: false, label:'Block3 description(max:180)' },
        image: { type: Types.CloudinaryImage, label:'Block3 image (614x351)' }
    }
});

Purpose.schema.virtual('Block1.full').get(function() {
    return this.content.extended || this.content.brief;
});
Purpose.schema.virtual('Block3.full').get(function() {
    return this.content.extended || this.content.brief;
});
Purpose.schema.virtual('Block2.full').get(function() {
    return this.content.extended || this.content.brief;
});

//Purpose.defaultColumns = 'title, state|20%, author|20%, publishedDate|20%';
Purpose.register();
