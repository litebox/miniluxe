var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Press Model
 **/

var Press = new keystone.List('Press',{
    map: { name: 'title' },
    autokey: { from: 'title', path: 'slug', unique: true }
});

Press.add({
    title: { type: String },
    state: { type: Types.Select, options: 'draft, published, archive', default: 'published', index: true },
    publishedDate: { type: Types.Date, index: true, dependsOn: { state: 'published' } },
    thumbnail_image: { type: Types.CloudinaryImage },
    title_image: { type: Types.CloudinaryImage },
    Url: { type: String },
    description: { type: Types.Html, wysiwyg: true, height: 150 }

});

Press.register();