var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Footer Nav Model
 * =============
 */

var Footernav = new keystone.List('Footer_Nav', {
    autokey: { from: 'name', path: 'key', unique: true },
    sortable:true
});

Footernav.add({
    name: { type: String, required: true },
    publishedDate: { type: Date, default: Date.now },
    state: { type: Types.Select, options: 'draft, published, archived', default: 'published', index: true },
    Text: { type: String },
    Url: { type: String }
});

Footernav.defaultColumns = 'name, state|20%';
Footernav.register();