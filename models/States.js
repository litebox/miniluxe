var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * State Model
 **/

var State = new keystone.List('State',{
    map: { name: 'title' },
    autokey: { from: 'title', path: 'slug', unique: true }
});

State.add({
    title: { type: String },
    state: { type: Types.Select, options: 'draft, published', default: 'published', index: true },
    Position:{type: String, required: false}



});

State.register();