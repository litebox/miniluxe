var keystone = require('keystone');
var Types = keystone.Field.Types;
var Admin = new keystone.List('Admin');

Admin.add({
    name: { type: Types.Name, required: true, index: true },
    email: { type: Types.Email, initial: true, required: true, index: true },
    password: { type: Types.Password, initial: true, required: true }
}, 'Permissions', {
    isAdmin: { type: Boolean, label: 'Can access Keystone', index: true }
});

// Provide access to Keystone
Admin.schema.virtual('canAccessKeystone').get(function() {
    return this.isAdmin;
});

Admin.schema.pre('save', function(next) {

    Admin.model.find()
        .where('email', this.email)
        .exec()
        .then(function (dupe) {
            if(dupe && dupe[0]) {
                next(Error('User is already the Email of ' + dupe[0].name));

            }
            else {
                next();
            }
        }, function (err) {
            next(err);
        });

});

Admin.defaultColumns = 'name, email, isAdmin';
Admin.register();