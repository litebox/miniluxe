var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Slider Model
 * =============
 */

var Slider = new keystone.List('Slider', {
	autokey: { from: 'name', path: 'key', unique: true }
});

Slider.add({
	name: { type: String, required: true },
	state: { type: Types.Select, options: 'draft, published, archived', default: 'draft', index: true },
	heroImage: { type: Types.CloudinaryImage, label:'heroImage(1600x550)'},
	slideurl: {type: Types.Url, hidden: true},
	title: {type: String},
	body: { type: Types.Textarea, label:'body(max:95)'},
	buttonText: {type: String},
	buttonUrl: { type: String },
	Position: { type: Types.Select, options: 'top-left, top-right, bottom-left, bottom-right', default: 'top-left' }
});

Slider.schema.pre('save', function(next) {
	this.slideurl = this._.heroImage.scale(1600, 550,{ quality: 100});


	next();
});

Slider.register();
