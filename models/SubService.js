/**
 * Created by balpreet on 1/7/2016.
 */
var keystone = require('keystone');
var Types = keystone.Field.Types;



var Subservice = new keystone.List('Subservice', {
    map: { name: 'title' },
    autokey: { from: 'title', path: 'slug', unique: true },
    sortable:true,
    sortContext: 'Service:subservices'
});

Subservice.add({
    title: { type: String, required:true },
    price: { type: Number },
    service:{ type: Types.Relationship, ref: 'Service', many: false, filters: { state: 'published' } },
    time: { type: Number , label:'Time (in minutes)',validate:[{ validator:isValidTime, msg:"Invalid time" }]},
    state: { type: Types.Select, options: 'draft, published', default: 'published', index: true },
    text: { type: String, label:'Text(max:235)' },
    image: { type: Types.CloudinaryImage, label:'For Nail Shapes (98x208)'},
    imageurl: {type: Types.Url, hidden: true}

});

Subservice.schema.pre('save', function(next) {
    this.imageurl = this._.image.scale(98, 208,{ quality: 100});


    next();
});

function isValidTime(value, done) {
    var numberonly=/^[0-9]*$/
    if(value){
        if (!numberonly.test(value)) {
            return done(null)

        }
        return done(1);
    }
    return done();
}

Subservice.defaultColumns = 'title, service|20%';
Subservice.register();