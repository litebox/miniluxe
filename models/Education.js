var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Education Model
 **/

var Education = new keystone.List('Education',{
    map: { name: 'title' },
    autokey: { from: 'title', path: 'slug', unique: true }
});

Education.add({
    title: { type: String, required: true },
    state: { type: Types.Select, options: 'draft, published', default: 'published', index: true },
    Position:{type: String, required: false}



});

Education.register();