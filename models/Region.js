var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
*
* Region Model
*
* */

var Region = new keystone.List('Region',{
    autokey: { from: 'name', path: 'key', unique: true }
});

Region.add({
    name: { type: String, required: true ,validate:[{ validator:isValidName, msg:"Please enter character only" }]},
    RID: { type: Types.Number },
    state: { type: Types.Select, options: 'draft, published, archived', default: 'published', index: true },
    NameLong: { type: String , validate:[{ validator:isValidName, msg:"invalid name long" }]},
    NameShort: { type: String ,validate:[{ validator:isValidName, msg:"invalid name short" }]}
});

Region.relationship({ ref: 'Location', path: 'locations' });

function isValidName(value, done) {
    var alphaonly=/^[a-zA-Z ]*$/
    if(value){
        if (!alphaonly.test(value)) {
            return done(null)

        }
        console.log('ltv');
        return done(1);
    }
    return done();
}

Region.register();