var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Career Model
 **/

var Career_position = new keystone.List('Career_position',{
    map: { name: 'title' },
    autokey: { from: 'title', path: 'slug', unique: true }
});

Career_position.add({
    title: { type: String },
    state: { type: Types.Select, options: 'draft, published', default: 'published', index: true },
    Position:{type: String, required: false}
});

Career_position.register();