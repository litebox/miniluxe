/**
 * Created by balpreet on 1/9/2016.
 */

var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Homepage Model
 **/

var Addon = new keystone.List('Add_on',{
    map: { name: 'title' },
    autokey: { from: 'title', path: 'slug', unique: true },
    sortable: true,
    sortContext: 'Service:add_ons'
});

Addon.add({
    title: { type: String , required:true },
    state: { type: Types.Select, options: 'draft, published', default: 'published', index: true },
    service:{ type: Types.Relationship, ref: 'Service', many: false, filters: { state: 'published' } },
    price:{type: Number, required: false}
});

Addon.defaultColumns = 'title, service|20%';

Addon.register();