/**
 * Created by balpreet on 1/9/2016.
 */

var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Homepage Model
 **/

var Service = new keystone.List('Service',{
    autokey: { from: 'name', path: 'key', unique: true }
});

Service.add({
    name: { type: String, required: true },
    state: { type: Types.Select, options: 'draft, published', default: 'published', index: true },
    image: { type: Types.CloudinaryImage, label:'Image(max:445x635)' },
    description:{type: String, required: false, label:'description(max:185)'}

});

Service.relationship({ ref: 'Subservice', path: 'subservices', sortOrder: true });
Service.relationship({ ref: 'Add_on', path: 'add_ons', sortOrder: true });

Service.defaultColumns = 'name, state|20%';

Service.register();