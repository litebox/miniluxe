var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Homepage Model
 **/

var Homepage = new keystone.List('Homepage',{
	nocreate: true,
	nodelete: true,
	map: { name: 'title' },
	autokey: { path: 'slug', from: 'title', unique: true }
});

Homepage.add({
	title: { type: String, required: true },
	state: { type: Types.Select, options: 'draft, published, archived', default: 'draft', index: true },
	service_one:{
		title: { type: String, required: false, label:'Service_one (Max char:12)', validate:[{ validator:isValidName, msg:"Only alphabetic character allowed  for Service One title" }] },
		image: { type: Types.CloudinaryImage, label:'Service_one image (135x190)' },
		link: { type: Types.Url }
	},
	service_two:{
		title: { type: String, required: false, label:'Service_two (Max char:12)', validate:[{ validator:isValidName, msg:"Only alphabetic character allowed for Service Two title" }] },
		image: { type: Types.CloudinaryImage , label:'Service_two image (95x190)' },
		link: { type: Types.Url }
	},
	service_three:{
		title: { type: String, required: false, label:'Service_three (Max char:12)', validate:[{ validator:isValidName, msg:"Only alphabetic character allowed for Service Three title" }] },
		image: { type: Types.CloudinaryImage, label:'Service_three image (190x129)'},
		link: { type: Types.Url }
	},
	service_four:{
		title: { type: String, required: false, label:'Service_four (Max char:12)', validate:[{ validator:isValidName, msg:"Only alphabetic character allowed for Service Four title" }] },
		image: { type: Types.CloudinaryImage, label:'Service_four image (174x190)' },
		link: { type: Types.Url }
	},
	giftcard: {
		title: { type: String, required: false },
		heading: { type: String, required: false },
		text: { type: String, label:'Giftcard text(max:165)' },
		image: { type: Types.CloudinaryImage, label:'Gift_Card image (570x340)' },
		buttontext: { type: String, label:'Gift-Card Button text (max:15)' },
		link: { type: String }
	},
	career: {
		title: { type: String, required: false },
		heading: { type: String, required: false },
		text: { type: String, label:'Career text(max:165)' },
		image: { type: Types.CloudinaryImage, label:'Career image (570x390)' },
		buttontext: { type: String, label:'Career Button text (max:15)' },
		link: { type: String }
	}

});

Homepage.schema.virtual('giftcard.full').get(function() {
	return this.content.extended || this.content.brief;
});

Homepage.schema.virtual('career.full').get(function() {
	return this.content.extended || this.content.brief;
});

function isValidName(value, done) {
	var alphaonly=/^[a-zA-Z ]*$/
	if(value){
		if (!alphaonly.test(value)) {
			return done(null)

		}
		return done(1);
	}
	return done();
}

Homepage.defaultColumns = 'title, state|20%';
Homepage.register();
