/**
 * Created by balpreet on 1/20/2016.
 */

var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Gift_card Model
 * =============
 */

var Gift_card = new keystone.List('Gift_card', {
    autokey: { from: 'name', path: 'key', unique: true }
});

Gift_card.add({
    name: { type: String, required: true },
    state: { type: Types.Select, options: 'draft, published', default: 'published', index: true },
    bannerImage: { type: Types.CloudinaryImage},
    slider: {
        image_first: { type: Types.CloudinaryImage },
        image_second: { type: Types.CloudinaryImage },
        image_third: { type: Types.CloudinaryImage },
        image_fourth: { type: Types.CloudinaryImage }
    },
    deluxeGift: { type: Types.CloudinaryImage }
});

Gift_card.register();