/**
 * Created by balpreet on 1/6/2016.
 */
var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Term_Condition Model
 * =============
 */

var Terms_Conditions = new keystone.List('Terms_Conditions', {
    autokey: { from: 'name', path: 'key', unique: true }
});

Terms_Conditions.add({
    name: { type: String, required: true },
    heroImage: { type: Types.CloudinaryImage, label:'heroImage (858x187)' },
    herourl: {type: Types.Url, hidden: true},
    text: {type: Types.Html, wysiwyg: true, height: 150}
});

Terms_Conditions.schema.pre('save', function(next) {
    this.herourl = this._.heroImage.scale(858, 187,{ quality: 100});


    next();
});

Terms_Conditions.register();