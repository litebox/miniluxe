/**
 * Created by balpreet on 1/9/2016.
 */

var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Face Model
 **/

var Face = new keystone.List('Face',{
    map: { name: 'title' },
    autokey: { from: 'title', path: 'slug', unique: true }
});

Face.add({
    title: { type: String },
    state: { type: Types.Select, options: 'draft, published', default: 'published', index: true },
    service:{ type: Types.Relationship, ref: 'Service', many: false, filters: { state: 'published' } },
    price:{type: Number, required: false}



});

Face.defaultColumns = 'title, service|20%';
Face.register();