var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * News Model
 * =============
 */

var News = new keystone.List('News', {
    autokey: { from: 'name', path: 'key', unique: true }
});

News.add({
    name: { type: String, required: true },
    state: { type: Types.Select, options: 'draft, published, archived', default: 'published', index: true },
    Short_headline: { type: String },
    Full_headline: { type: String },
    Date: { type: Types.Date },
    Thumbnail: { type: Types.CloudinaryImages },
    Url: { type: String }
});

News.register();