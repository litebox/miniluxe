/**
 * Created by balpreet on 1/7/2016.
 */
var keystone = require('keystone');
var Types = keystone.Field.Types;



var Location = new keystone.List('Location', {
    map: { name: 'nameLong' },
    autokey: { from: 'nameLong', path: 'slug', unique: true }
});

Location.add({
    nameLong: { type: String, required: true  },
    nameShort: { type: String },
    B4TID: { type: Number },
    Status: { type: Types.Select, options: 'draft, published', default: 'published', index: true },
    description: { type: String },
    address1: { type: String, label:'Address(max-char:55)' },
    address2: { type: String, label:'Address(max-char:120)' },
    region:{type: Types.Relationship, ref: 'Region', many: false, filters: { state: 'published' }},
    city: { type: String, required: false },
    state: { type: String, required: false },
    zipcode: { type: Number, required: false, validate:[{ validator:isValidZip, msg:"Invalid zip code" }]},
    phone1: { type: String, required: false, note:'phone format(xxx-xxx-xxxx)', validate:[{ validator:isValidPhone, msg:"invalid phone1 format" }] },
    phone2: { type: String, required: false, note:'phone format(xxx-xxx-xxxx)', validate:[{ validator:isValidPhone, msg:"invalid phone2 format" }] },
    HoursHTML: {type: Types.Html, wysiwyg: true, height: 150, label:'HoursHTML(max:30)'},
    ThumbnailImage: { type: Types.CloudinaryImage, label:'ThumbnailImage(375x250)' },
    Thumbnail: {type: Types.Url, hidden: true},
    heroImage1: { type: Types.CloudinaryImage, label:'heroImage1(850x550)' },
    hero1url: {type: Types.Url, hidden: true},
    heroImage2: { type: Types.CloudinaryImage, label:'heroImage2(850x550)' },
    hero2url: {type: Types.Url, hidden: true},
    heroImage3: { type: Types.CloudinaryImage, label:'heroImage3(850x550)' },
    hero3url: {type: Types.Url, hidden: true},
    GPS_X: { type: Number, required: false },  //lat
    GPS_Y: { type: Number, required: false }
});

Location.schema.pre('save', function(next) {
    this.Thumbnail = this._.ThumbnailImage.fill(375, 250,{ quality: 100});
    this.hero1url = this._.heroImage1.fill(850, 550,{ quality: 90});
    this.hero2url = this._.heroImage2.fill(850, 550,{ quality: 90});
    this.hero3url = this._.heroImage3.fill(850, 550,{ quality: 90});
    next();
});

function isValidZip(value, done) {
    var alphaonly=/^[0-9]*$/
    if(value){
        if (!alphaonly.test(value) || value>99999) {
            return done(null)

        }
        return done(1);
    }
    return done();
}
function isValidPhone(value, done) {
    var alphaonly=/^\d{3}-\d{3}-\d{4}$|^\d{10}$/
    if(value){
        if (!alphaonly.test(value)) {
            return done(null)

        }
        // console.log('ltv');
        return done(1);
    }
    return done();
}
Location.register();